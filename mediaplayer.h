#ifndef MEDIAPLAYER_H
#define MEDIAPLAYER_H

#include <QObject>
#include <QVariant>

class MediaPlayer : public QObject
{
    Q_OBJECT
public:
    explicit MediaPlayer(QObject *parent = nullptr);
    Q_INVOKABLE void playSound();

signals:

public slots:
};

#endif // MEDIAPLAYER_H
