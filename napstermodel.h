#ifndef NAPSTERMODEL_H
#define NAPSTERMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QVariant>

/*
Exposable class for returning songs from HTTP API.
  */

class NapsterModel
{
    NapsterModel();
};
/*
class NapsterModel : public QAbstractListModel
{
public:
    NapsterModel();
    Q_INVOKABLE void appendDummy();
    Q_INVOKABLE void append(const QString& name, const qreal& price, const qreal& quantity);
    Q_INVOKABLE QVariant get(const qint32& index, const QString& key);
    Q_INVOKABLE void set(const qint32& index, const QString& key, const QVariant& value);
    Q_INVOKABLE void remove(const qint32& index);
    virtual int rowCount(const QModelIndex &parent) const;
    // TODO: flags (must return Qt::ItemIsEditable), setData
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);
    virtual Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;
};*/

#endif // NAPSTERMODEL_H
