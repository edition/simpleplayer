import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.0
import QtMultimedia 5.12
import "./goodies.js" as Util

Window {
    id: window
    visible: true
    width: 640
    height: 480
    color: "#000000"
    title: qsTr("Hello World")
    property int globalAppFontSize : 28

    //TODO: Make this global
    MediaPlayer {
        id: player
        source: "qrc:/Piano.mp3"
    }

    ColumnLayout {
        x: 60
        y: 150
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        GridLayout {
            rows: 1
            columns: 3

            Button {
                id: buttonPlay
                text: qsTr("Play")
                Layout.fillHeight: true
                Layout.columnSpan: 1
                Layout.preferredHeight: 66
                Layout.preferredWidth: 166
                font.pointSize: globalAppFontSize
                onClicked: { player.play(); }
            }

            Button {
                id: buttonPause
                text: qsTr("Pause")
                Layout.fillHeight: true
                Layout.rowSpan: 1
                Layout.preferredHeight: 35
                Layout.preferredWidth: 181
                font.pointSize: globalAppFontSize
                onClicked: { player.pause(); }
            }

            Button {
                id: buttonStop
                text: qsTr("Stop")
                Layout.fillHeight: true
                Layout.preferredHeight: 35
                Layout.preferredWidth: 162
                font.pointSize: globalAppFontSize
                onClicked: { player.stop(); }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Text {
                id: element
                color: "#ffffff"
                text: {
                    var x = player.position / 1000;
                    return Util.pad( (x / 60).toFixed(0), 2, 0 ) + ":" + Util.pad( (x % 59).toFixed(0), 2, 0 );
                }
                fontSizeMode: Text.HorizontalFit
                font.pointSize: globalAppFontSize
            }

            Slider {
                id: slider
                to: player.duration
                value: player.position
                onPressedChanged: {
                    player.seek(value);
                }
            }
        }
    }
}
