#include "mediaplayer.h"
#include <QMediaPlayer>

MediaPlayer::MediaPlayer(QObject *parent) : QObject(parent)
{

}

//test function for more to come
void MediaPlayer::playSound()
{
    QMediaPlayer* player = new QMediaPlayer;
    //connect(player, SIGNAL(positionChanged(qint64)), this, SLOT(positionChanged(qint64)));
    player->setMedia(QUrl::fromLocalFile("qrc:/Piano.mp3"));
    player->setVolume(50);
    player->play();
}
